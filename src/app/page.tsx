"use client";
import React, { useEffect, useState, useRef } from "react";

const Breadcrumb = () => (
  <div className="flex items-center space-x-2 text-gray-500 text-sm">
    <a href="#" className="hover:underline">
      Exchange
    </a>
    <span>&gt;</span>
    <span>Order Book</span>
  </div>
);

const Header = () => (
  <h1 className="text-3xl font-semibold mt-4">
    Order Book <span className="text-gray-500"></span>
  </h1>
);

interface Order {
  side: string;
  price: number;
  amount: number;
  total: number;
  sum: number;
}

interface Props {
  orders: Order[];
  type: string;
  color: string;
}

const OrderTable: React.FC<Props> = ({ orders, type, color }) => (
  <div className="bg-white shadow rounded-lg p-4">
    <h2 className={`text-lg font-semibold mb-4 text-${color}-600`}>
      {type} Order
    </h2>
    <table className="w-full text-left">
      <thead>
        <tr className="text-gray-500 text-sm">
          <th className="py-2">Side</th>
          <th className="py-2">Price (USDT)</th>
          <th className="py-2">Amount (BTC)</th>
          <th className="py-2">Total (USDT)</th>
          <th className="py-2">Sum (USDT)</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order, index) => (
          <tr key={index} className={index % 2 === 0 ? `bg-${color}-600` : ""}>
            <td className={`py-2 text-${color}-600`}>{order.side}</td>
            <td className="py-2">{order.price}</td>
            <td className="py-2">{order.amount}</td>
            <td className="py-2">{order.total}</td>
            <td className="py-2">{order.sum}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

const OrderBook = () => {
  const [selectedToken, setSelectedToken] = useState("BTCUSDT");
  const [orderBook, setOrderBook] = useState({ bids: [], asks: [] });
  const handleTokenChange = (event:  React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedToken(event.target.value);
  };
  const selectedTokenRef = useRef(selectedToken);

  useEffect(() => {
    selectedTokenRef.current = selectedToken;
  }, [selectedToken]);
  useEffect(() => {
    const fetchOrderBook = async () => {
      try {
        console.log({ selectedToken });
        const response = await fetch(
          `https://api4.binance.com/api/v3/depth?symbol=${selectedTokenRef.current}&limit=100`
        );
        const data = await response.json();
        console.log(data);
        setOrderBook(data);
        const interval = setInterval(fetchOrderBook, 5000); // Refresh every 5 seconds

        return () => clearInterval(interval); // Cleanup interval on component unmount
      } catch (error) {
        console.error("Error fetching order book:", error);
      }
    };

    fetchOrderBook();
  }, [selectedToken]);
  

  const buyOrders = orderBook.bids.map((item, index) => {
    const price = parseFloat(item[0]);
    const amount = parseFloat(item[1]);
    const total = price * amount;
    const sum = total;
    return { side: `Buy ${index + 1} `, price, amount, total, sum };
  });


  const sellOrders = orderBook.asks.map((item, index) => {
    const price = parseFloat(item[0]);
    const amount = parseFloat(item[1]);
    const total = price * amount;
    const sum = total;
    return { side: `Sell ${index + 1} `, price, amount, total, sum };
  });

  return (
    <div className="bg-gray-50 min-h-screen font-inter">
      <div className="container mx-auto p-6">
        <Breadcrumb />
        <Header />
        <select value={selectedToken} onChange={handleTokenChange}>
          <option value="BTCUSDT">BTC/USDT</option>
          <option value="ETHUSDT">ETH/USDT</option>
        </select>
        <div className="grid grid-cols-2 gap-6 mt-6">
          <OrderTable orders={buyOrders} type="Buy" color="green" />
          <OrderTable orders={sellOrders} type="Sell" color="red" />
        </div>
      </div>
    </div>
  );
};

export default OrderBook;
